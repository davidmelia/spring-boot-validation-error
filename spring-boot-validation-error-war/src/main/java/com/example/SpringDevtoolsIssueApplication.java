package com.example;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@SpringBootApplication
public class SpringDevtoolsIssueApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDevtoolsIssueApplication.class, args);
	}

	@Autowired
	private org.springframework.validation.beanvalidation.LocalValidatorFactoryBean validator;

	@Bean
	public LocalValidatorFactoryBean validator() {
		return new LocalValidatorFactoryBean();
	}

	@Bean
	public FilterRegistrationBean create() {
		FilterRegistrationBean bean = new FilterRegistrationBean();
		bean.setFilter(new Filter() {

			@Override
			public void init(FilterConfig filterConfig) throws ServletException {
				// TODO Auto-generated method stub

			}

			@Override
			public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
					throws IOException, ServletException {
				// TODO Auto-generated method stub

			}

			@Override
			public void destroy() {
				// TODO Auto-generated method stub

			}
		});
		return bean;

	}

}
